﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccionesPersonaje : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("Empezamos el movimiento");
	}
	
	// Update is called once per frame
	void Update () {
        Andar();
        Saltar();
    }
    public void Saltar() {
        if(Input.GetKeyDown("space")) {
            transform.Translate(Vector3.up * 5 * Time.deltaTime, Space.World);
        }
    }
    public void Andar() {
        var x = -(Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f);
        var z = -(Input.GetAxis("Vertical") * Time.deltaTime * 3.0f);

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
    }
}
